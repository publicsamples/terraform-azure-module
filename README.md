# Terraform Azure Module
## O que esse módulo faz?
* Cria um resource group
## Como usar

```terraform
module "resource_group" {
  source      = "git::ssh://git@gitlab.com/publicsamples/terraform-azure-module.git"
  name        = "${var.prefix}_${var.project_name}_resource_group"
  location    = var.location
  environment = var.environment  
}
```

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| name | Nome do grupo de recursos. | string | n/a | yes |
| location | Localização do grupo de recurso | string | n/a| yes |
| environment | Tipo do ambiente desse grupo de recurso | string | n/a | yes |

## outputs

| Name | Description |
|------|-------------|
| id" | ID do grupo de recurso. |
