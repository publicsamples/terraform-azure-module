###
### Resource Group - Responsável por separar os grupos de recursos que serão usados por ambientes
### 
# https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/resource_group

resource "azurerm_resource_group" "resource_group" {
  name          = var.name
  location      = var.location
  tags = {
    terraform      = "true"
    environment = "${var.environment}"
  }
}