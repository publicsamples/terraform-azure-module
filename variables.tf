variable "name" {
  type = string
  description = "Name of resource group"
}
variable "location" {
  type = string
  description = "Location of resource group"
}

variable "environment" {
    type = string
    description = "Which environment the resource group was intended for"
}